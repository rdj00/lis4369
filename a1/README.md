> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and BitBucket
2. Development Installations
3. Questions
4. Bitbucket Repo links:
    - https://bitbucket.org/rdj00/lis4369/src/master/
    - https://bitbucket.org/rdj00/bitbucketstationlocations/src/master/ 
    

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1.ipynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb)
* git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - This command creates an empty Git repository - basically a .git directory with            subdirectories for objects, refs/heads, refs/tags, and template files. An initial branch without any commits will be created (see the --initial-branch option below for its name).
2. git status - Displays paths that have differences between the index file and the current HEAD commit, paths that have differences between the working tree and the index file, and paths in the working tree that are not tracked by Git (and are not ignored by gitignore[5]).
3. git add - This command updates the index using the current content found in the working tree, to prepare the content staged for the next commit.
4. git commit - Create a new commit containing the current contents of the index and the given log message describing the changes.
5. git push - Updates remote refs using local refs, while sending objects necessary to complete the given refs.
6. git pull - Incorporates changes from a remote repository into the current branch. 
7. git show - Shows one or more objects (blobs, trees, tags and commits).

#### Assignment Screenshots:

#### Screenshot of a1_tip_calculator running(IDLE):

![Python Installation Screenshot IDLE](img/a1_tip_calculator_idle.png "A1 IDLE Screenshot")

#### Screenshot of a1_tip_calculator running(VS Code):

![Python Installation Screenshot VS Code](img/a1_tip_calculator_vs_code.png "A1 VS Code Screenshot")

#### A1 Jupyter Notebook:

![Python Installation Screenshot Jupyter Notebook](img/a1_jupyter_notebook.png "A1 VS Code Screenshot")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
