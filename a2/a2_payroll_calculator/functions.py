def get_title():
      print ("\nDeveloper: Ryan Johns\n"
      +"Course: LIS4369\n"
      +"Semester: Fall 2021\n\n\n"
      +"Payroll Calculator")

def get_requirements():
    print("\nProgram Requirements:\n"
      + "1. Must use float data type for user input.\n"
      + "2. Overtime rate: 1.5 times hourly rate (hours over 40)\n"
      + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
      + "4. Must format currency with dollar sign, and round to two decimal places.\n"
      + "5. Create at least three functions that are called by the program:\n"
      +"\ta. main(): calls atleast two other functions.\n"
      +"\tb. get_requirements(): displays the program requirements.\n"
      +"\tc. calculate_payroll(): calculates an individual one-week paycheck.\n")

def print_pay(pay_roll,over_time,holidaypay,grosspay): 
    print("\nOutput:")
    print("{0:<10} ${1:,.2f}".format("Base:",pay_roll))
    print("{0:<10} ${1:,.2f}".format("Overtime:",over_time))
    print("{0:<10} ${1:,.2f}".format("Holiday:",holidaypay))
    print("{0:<10} ${1:,.2f}".format("Gross:",grosspay))

def calculate_payroll(hours_worked,holiday_hours,pay_rate):
    base_hours = 40
    holiday_rate = 2.0
    ot_rate = 1.5
    
    if (hours_worked <= base_hours):
        payroll = hours_worked*pay_rate
        holiday_pay = (pay_rate * holiday_rate) * holiday_hours
        overtime = 0
        gross_pay = payroll + holiday_pay + overtime
        print_pay(payroll,overtime,holiday_pay,gross_pay)
        
    else:
        payroll = base_hours*pay_rate
        holiday_pay = (pay_rate * holiday_rate) * holiday_hours
        overtime = (hours_worked - base_hours)* pay_rate * ot_rate
        gross_pay = payroll + holiday_pay + overtime
        print_pay(payroll,overtime,holiday_pay,gross_pay)
