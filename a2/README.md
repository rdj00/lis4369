> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Assignment 2 Requirements:

*Development:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules(See Ch. 4): 
3. Be sure to test your program using both IDLE and Visual Studio Code.
4. *Be Sure* to carefully review (How to Write Python): https://realpython.com/lessons/what-pep-8-and-why-you-need-it/
    

#### README.md file should include the following items:

* Assignment requirements, as per A2.
* Screenshots as per examples below.
* Upload A2 .ipynbfile andcreate link in README.md; Note: *Before*  uploading .ipynbfile, *be sure* to do the following actions from Kernel menu:
    * Restart & Clear Output
    * Restart & Run All




#### Assignment Screenshots:

#### Payroll Calculator no Overtime:

![Python Installation Screenshot VS Code](img/a2_payroll_calculator_vs_code.png "Payroll calculator no overtime")

#### Payroll Calculator with Overtime:

![Python Installation Screenshot VS Code](img/a2_payroll_calculator_vs_code2.png "Payroll calculator with overtime")

#### a2_payroll.ipynb:

![Python Installation Screenshot Jupyter Notebook](img/a2_jupyter_notebook.png "Payroll calculator no overtime")

#### a2_payroll.ipynb PART 2:

![Python Installation Screenshot Jupyter Notebook](img/a2_jupyter_notebook2.png "Payroll calculator with overtime") 

#### Skillset 1: Square Feet to Acres Screenshot:

![Python Installation Screenshot VS Code](img/ss1_square_feet_to_acres.png "Square Feet to Acres Screenshot")

#### Skillset 2: Miles per Gallon Screenshot:

![Python Installation Screenshot VS Code](img/ss2_miles_per_gallon.png "Miles per Gallon Screenshot")

#### Skillset 3: IT/ICT Student Percentage Screenshot:

![Python Installation Screenshot VS Code](img/ss3_it_ict_student_percentage.png "IT/ICT Student Percentage Screenshot")