> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Assignment 2 Requirements:

*Development:*

1. Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial (includes A5 requirements): Save as: learn_to_use_r.R 
2.  Code and run lis4369_a5.R (see below). Include link to file in a5 README.md file. 
3. Include at least two 4-panel RStudio screenshots : 1) learn_to_use_r.R, and 2) lis4369_a5.R code. 
4. Also, be sure to include at least four plots—that is, at least two plots for the tutorial, and two plots for the assignment file (below), in your README.md file. 
5. Be sure to test your program using RStudio. 

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots as per examples below.
* Upload A2 .ipynbfile andcreate link in README.md; Note: *Before*  uploading .ipynbfile, *be sure* to do the following actions from Kernel menu:
    * Restart & Clear Output
    * Restart & Run All




#### Assignment Screenshots:

#### learn_to_use_r.R Tutorial:

![learn_to_use_r.R Tutorial Screenshot ](img/learn_to_use_r.png "learn_to_use_r.R Tutorial Screenshot ")

#### learn_to_use_r.R  (Plot1):

![learn_to_use_r.R  (Plot1)](img/plot1.png "learn_to_use_r.R  (Plot1)")

#### learn_to_use_r.R  (Plot2):

![learn_to_use_r.R  (Plot2)](img/plot2.png "learn_to_use_r.R  (Plot2)")

#### lis4369_a5.R:

![lis4369_a5.R (RStudio 4-panel Screenshot) ](img/lis4369_a5.R.png "lis4369_a5.R (RStudio 4-panel Screenshot) ")

#### lis4369_a5.R (Graph1):

![lis4369_a5.R (Graph1)](img/R.graph1.png "lis4369_a5.R (Graph1)")

#### lis4369_a5.R (Graph2):

![lis4369_a5.R (Graph2)](img/R.graph2.png "lis4369_a5.R (Graph2)")

#### Skillset 13 Sphere Volume Calculator:

![Skillset 13 Screenshot](img/ss13_sphere_volume_calculator.png "Skillset 13 Sphere Volume Calculator") 

#### Skillset 14 Calculator with Error Handling:

![Skillset 14 Screenshot](img/ss14_calculator_with_error_handling.png "Skillset 14 Calculator with Error Handling")

#### Skillset 15 Read Write File:

![Skillset 15 Screenshot](img/ss15_read_write_file.png "Skillset 15 Read Write File") 