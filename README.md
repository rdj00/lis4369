> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions (Python)

## Ryan Johns

### LIS4369 Requirements:

*Course Work Links:*

1. [A1_README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create "a1_tip_calculator" application
    - Create "a1 tip calculator" Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo 
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2_README.md](a2/README.md "My A2 README.md file")
    - Create "a2_payroll_calculator" application
    - Create "a2_payroll_calculator" Jupyter Notebook
    - Be sure to test your program using both IDLE and Visual Studio Code.
    - Provide screenshots of installations

3. [A3_README.md](a3/README.md "My A3 README.md file")
    - Create "a3_painting_estimator" application
    - Create "a3_painting_estimator" Jupyter Notebook
    - Be sure to test your program using Visual Studio Code.
    - Provide screenshots of installations

4. [A4_README.md](a4/README.md "My A4 README.md file")
    - Create "a4_data_analysis2" application
    - Create "a4_data_analysis2" Jupyter Notebook
    - Be sure to test your program using Visual Studio Code.
    - Provide screenshots of installations

5. [A5_README.md](a5/README.md "My A5 README.md file")
    - Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial (includes A5 requirements): Save as: learn_to_use_r.R 
    - Code and run lis4369_a5.R (see below). Include link to file in a5 README.md file. 
    - Include at least two 4-panel RStudio screenshots : 1) learn_to_use_r.R, and 2) lis4369_a5.R code. 
    - Also, be sure to include at least four plots—that is, at least two plots for the tutorial, and two plots for the assignment file (below), in your README.md file. 
    - Be sure to test your program using RStudio. 

6. [P1_README.md](p1/README.md "My P1 README.md file")
    - Code and run demo.py.
    - Use it to backward-engineer the screenshots below it.
    - Update conda, and install necessary Python packages. Invoke commands below. 
    - Log in as administrator(PC:Anaconda command prompt, LINUX/Mac:sudo)
        - conda update conda
        - conda update --all
        - pip install pandas-datareader
    - Be sure to test your program using both IDLE and Visual Studio Code.




7. [P2_README.md](p2/README.md "My P2 README.md file")
    - Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
    - Save as lis4369_p2.R. Include link to file in p2 README.md file. 
    - Include link to lis4369_p2_output.txt file 
    - Include at least one 4-panel RStudio screenshot executing lis4369_p2.R code.
    - Also, be sure to include at least two plots (*must* include *your* name in plot titles), in your *README.md file*. See examples below.


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
