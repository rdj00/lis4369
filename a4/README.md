> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Assignment 4 Requirements:

*Development:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules(See Ch. 4): 
3. Be sure to test your program using both IDLE and Visual Studio Code.
4. *Be Sure* to carefully review (How to Write Python): https://realpython.com/lessons/what-pep-8-and-why-you-need-it/
    

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots as per examples below.
* Upload A4 .ipynbfile andcreate link in README.md; Note: *Before*  uploading .ipynbfile, *be sure* to do the following actions from Kernel menu:
    * Restart & Clear Output
    * Restart & Run All




#### Assignment Screenshots:

#### Data Analysis 2 with Graph included (VS Code would not allow me to scroll to the top):

![Python Installation Screenshot VS Code](img/a4_data_analysis2.png "Data Analysis 2 with graph")


#### a4 ipynb:

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook.png "Data Analysis 2")

#### a4 ipynb PART 2:

![Python Installation Screenshot Jupyter Notebook](img/a4_jupyter_notebook2.png "Data Analysis 2") 

#### Skillset 10 Using Dictionaries:

![Python Installation Screenshot Jupyter Notebook](img/ss10_using_dictionaries.png "Skillset 10 Python Dictionaries") 

#### Skillset 11 Random Number Generator:

![Python Installation Screenshot Jupyter Notebook](img/ss11_random_num_generator.png "Skillset 11 Random Number Generator") 

#### Skillset 12 Temperature Conversion Program:

![Python Installation Screenshot Jupyter Notebook](img/ss12_temperature_conversion.png "Skillset 12 Temperature Conversion") 