#!/usr/bin/env python3


def title():
      print ("\nDeveloper: Ryan Johns\n"
      +"Course: LIS4369\n"
      +"Semester: Fall 2021\n\n\n"
      +"Payroll Calculator")

def get_requirements():
      title()
      print("\nProgram Requirements:\n"
      + "1. Must use float data type for user input.\n"
      + "2. Overtime rate: 1.5 times hourly rate (hours over 40)\n"
      + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
      + "4. Must format currency with dollar sign, and round to two decimal places.\n"
      + "5. Create at least three functions that are called by the program:\n"
      +"\ta. main(): calls atleast two other functions.\n"
      +"\tb. get_requirements(): displays the program requirements.\n"
      +"\tc. calculate_payroll(): calculates an individual one-week paycheck.\n")

def print_pay(pay_roll,over_time,holidaypay,grosspay): 
            print("Base:\t\t","${0:,.2f}".format(pay_roll))
            print("Overtime:\t","${0:,.2f}".format(over_time))
            print("Holiday:\t","${0:,.2f}".format(holidaypay))
            print("Gross:\t\t","${0:,.2f}".format(grosspay))

def calculate_payroll(hours_worked,holiday_hours,pay_rate):
      if (hours_worked <= 40):
            payroll = hours_worked*pay_rate
            holiday_pay = (pay_rate * 2.0) * holiday_hours
            overtime = 0
            gross_pay = payroll + holiday_pay + overtime
            print_pay(payroll,overtime,holiday_pay,gross_pay)
            
      elif (hours_worked >= 41):
            regular_hours = 40
            payroll = regular_hours*pay_rate
            holiday_pay = (pay_rate * 2.0) * holiday_hours
            overtime = (hours_worked - 40)* pay_rate * 1.5
            gross_pay = payroll + holiday_pay + overtime
            print_pay(payroll,overtime,holiday_pay,gross_pay)


def main():
      get_requirements()

      print("\nInput:")
      hours = float(input("Enter hours worked: "))
      holiday = float(input("Enter holiday hours: "))
      payrate = float(input("Enter holiday pay rate: "))

      print("\nOutput:")
      calculate_payroll(hours,holiday,payrate)
      
main()