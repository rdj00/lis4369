> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Project 2 Requirements:

*Development:*

1. Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
2. Save as [lis4369_p2.R](lis4369_p2/lis4369_p2.R "My lis4369_p2.R file") Include link to file in p2 README.md file. 
3. Include link to [lis4369_p2_output.txt ](lis4369_p2/lis4369_p2_output.txt  "My lis4369_p2_output.txt file")file 
4. Include at least one 4-panel RStudio screenshot executing lis4369_p2.R code.
5. Also, be sure to include at least two plots (*must* include *your* name in plot titles), in your *README.md file*. See examples below.
    

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots of output.


#### Project 2 Screenshots:

#### lis4369_p2.R (RStudio 4-panel Screenshot):

![lis4369_p2.R (RStudio 4-panel Screenshot)](img/p2.png "4-Panel Screenshot RStudio")

#### Example Graphs:

![Example Graph 1](img/P2plot1.png "Example Graph 1")

![Example Graph 2](img/P2plot2.png "Example Graph 2")

