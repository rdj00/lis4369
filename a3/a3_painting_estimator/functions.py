def get_title():
      print ("\nDeveloper: Ryan Johns\n"
      +"Course: LIS4369\n"
      +"Semester: Fall 2021\n\n\n"
      +"Painting Estimator")



def get_requirements():
    print("\nProgram Requirements:\n"
      + "1. Calculator home interior paint cost (w/o primer).\n"
      + "2. Must use float data types.\n"
      + "3. Must use SQFT_PER_GALLON constant (350).\n"
      + "4. Must use iteration structure (aka loop).\n"
      + "5. Format, right-align numbers, an round to two decimal places.\n"
      + "6. Create atleast five functions that are called by the program:"
      +"\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost()\n"
      +"\tb. get_requirements(): displays the program requirements.\n"
      +"\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
      +"\td. print_painting_estimate(): displays painting costs."
      +"\te. print painting_percentage(): displays painting costs percentages.")

    


def estimate_painting_cost(sqft_total,price_gallon,rate_hourly):
 

    SQFT_PER_GALLON = 350

    gallons = sqft_total / SQFT_PER_GALLON 
    labor = rate_hourly * sqft_total
    paint = price_gallon * gallons

    total = paint + labor
    paint_percentage = (paint / total) * 100
    labor_percentage = (labor / total) * 100

    print_painting_estimate(sqft_total, SQFT_PER_GALLON, gallons, price_gallon, rate_hourly)
    print_painting_percentage(paint, labor, total, paint_percentage, labor_percentage)

def print_painting_estimate(sqft_total, SQFT_PER_GALLON, gallons, price_gallon, rate_hourly):
    
    print("\nOutput:")
    print("{0:<10} {1:>20}".format("Item", "Amount"))
    print("{0:<10} {1:>18,.2f}".format("Total Sq Ft: ", sqft_total))
    print("{0:<10} {1:>13,.2f}".format("Sq Ft per Gallon: ", SQFT_PER_GALLON))
    print("{0:<10} {1:>12,.2f}".format("Number of Gallons: ", gallons))
    print("{0:<10}     $ {1:>7,.2f}".format("Paint per Gallon: ", price_gallon))
    print("{0:<10}      $ {1:>7,.2f}".format("Labor per Sq Ft: ", rate_hourly))
    
  
def print_painting_percentage(paint, labor, total, paint_percentage, labor_percentage):
    total_percentage = 100
    print("{0:<9} {1:>10} {2:>14}".format("\nCost","Amount","Percentage"))
    print ("{0:<9} ${1:>8,.2f} {2:>13.2f}%". format("Paint: ", paint, paint_percentage))
    print ("{0:<9} ${1:>4,.2f} {2:>13.2f}%". format("Labor: ", labor, labor_percentage))
    print("{0: <9} ${1:>4,.2f} {2:>13.2f}%". format ("Total:", total, total_percentage))

