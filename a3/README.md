> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Assignment 3 Requirements:

*Development:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules(See Ch. 4):
    * **functions.py** module contains the following functions:
        * get_requirements()
        * estimate_painting_cost()
        * print_painting_estimate()
        * print_painting_percentage()
    * **main.py** module imports the **functions.py** module, and calls the functions.
3. Be sure to test your program using both IDLE and Visual Studio Code.
4. *Be Sure* to carefully review (How to Write Python): https://realpython.com/lessons/what-pep-8-and-why-you-need-it/
    

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots as per examples below.
* Upload A3 .ipynbfile andcreate link in README.md; Note: *Before*  uploading .ipynbfile, *be sure* to do the following actions from Kernel menu:
    * **Restart & Clear Output**
    * **Restart & Run All**




#### Assignment Screenshots:

#### Painting Estimator:

![Python Installation Screenshot VS Code](img/a3_painting_estimator_vs_code2.png "Painting Estimator")

![Python Installation Screenshot VS Code](img/a3_painting_estimator_vs_code1.png "Painting Estimator")

#### Painting Estimator Jupyter Notebook:

![Python Installation Screenshot Jupyter Notebook](img/a3_jupyter_notebook1.png "Painting Estimator")

![Python Installation Screenshot Jupyter Notebook](img/a3_jupyter_notebook2.png "Painting Estimator")

#### Skillset 4: Calorie Percentage:

![Python Installation Screenshot VS Code](img/ss4_calorie_percentage.png "Calorie Percentage")

#### Skillset 5: Python Selection Structures:

![Python Installation Screenshot VS Code](img/ss5_python_selection_structures.png "Python Selection Structures")

#### Skillset 6: Python Loops:

![Python Installation Screenshot VS Code](img/ss6_python_loops.png "Python Loops")
