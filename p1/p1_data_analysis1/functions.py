def get_title():
      print ("\nDeveloper: Ryan Johns\n"
      +"Course: LIS4369\n"
      +"Semester: Fall 2021\n\n\n"
      +"Data Analysis 1")


def get_requirements():
    print("\nProgram Requirements:\n"
      + "1. Run demo.py.\n"
      + "2. If errors, more than likely missing installations.\n"
      + "3. Test Python Package Installer: pip freeze.\n"
      + "4. Research how to do the following installations:\n"
      + "\ta. pandas (only if missing)\n"
      + "\tb. pandas-datareader (only if missing)\n"
      + "\tc. matplotlib (only if missing)\n"
      + "5. Create at least three functions that are called by the program:\n"
      + "\ta. main(): calls at least two other functions.\n"
      + "\tb. get_requirements(): displays the program requirements.\n"
      + "\tc. data_analysis_1(): displays the following data.\n")

def data_analysis_1():
    import datetime
    import pandas_datareader as pdr
    import matplotlib.pyplot as plt
    from matplotlib import style

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)


    df = pdr.DataReader("XOM","yahoo",start,end)

    print("\nPrint number of records: ") 
    print(len(df.index))

    print("\n")
    print(df.columns)

    print("\nPrint data frame: ") 
    print(df)

    print("\nPrint first five lines:")
    print(df.head(5))

    print("\nPrint last five lines:")
    print(df.tail(5))

    print("\nPrint first 2 lines:")
    print(df.head(2)) 

    print("\nPrint last 2:")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()   




      