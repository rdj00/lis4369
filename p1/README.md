> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ryan Johns

### Project 1 Requirements:

*Development:*

1. Requirements:
    1. Code and run demo.py.(Note: *be sure* necessary packages are installed!)
    2. Use it to backward-engineer the screenshots below it.
    3. Update conda, and install necessary Python packages. Invoke commands below.
        - conda updateconda
        - conda update --all
        - pip install pandas-datareader
    4. Create at least three functions that are called by the program:
        - main(): calls at least two other functions.
        - get_requirements(): displays the program requirements.
        - data_analysis_1(): displays the following data.
2. Be sure to test your program using both IDLEand Visual Studio Code.
        
    

#### README.md file should include the following items:

* Assignment requirements, as per A1.
* Screenshots as per examples below.
* Upload P1 .ipynb file andcreate link in README.md; Note: *Before*  uploading .ipynb file, *be sure* to do the following actions from Kernel menu:
    * Restart & Clear Output
    * Restart & Run All




#### Assignment Screenshots:

#### Data Analysis 1

![Python Installation Screenshot VS Code](img/p1_data_analysis1.png "Data Analysis 1")

![Python Installation Screenshot VS Code](img/p1_data_analysis1.2.png "Data Analysis 1")

#### Graph:

![Python Installation Screenshot VS Code](img/p1_graph.png "Data Analysis 1 Graph") 

#### Data Analysis 1 Jupyter Notebook 

![Python Installation Screenshot VS Code](img/p1_jupyter_notebook.png "Data Analysis 1 Jupyter Notebook") 

![Python Installation Screenshot VS Code](img/p1_jupyter_notebook2.png "Data Analysis 1 Jupyter Notebook")  

#### Skillset 7 Python Lists

![Python Installation Screenshot VS Code](img/ss7_screenshot.png "Skillset 7 Python Lists")  

#### Skillset 8 Python Tuples

![Python Installation Screenshot VS Code](img/ss8_screenshot.png "Skillset 8 Python Tuples")  

#### Skillset 9 Python Sets

![Python Installation Screenshot VS Code](img/ss9_screenshot.png "Skillset 9 Python Sets")