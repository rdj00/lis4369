#!/usr/bin/env python3

# Program requirements
print("Skill Set 2 - Mile Per Gallon")

# Developer: Ryan Johns

# Course: LIS4369

# Semester: Fall 2021 

print("\nProgram Requirements:\n"
      + "1. Convert MPG\n"
      + "2. Must use float data type for user input and calculation.\n"
      + "3. Format and round conversion to two decimal places\n")

print("\nUser Input:")
miles_driven = float(input("Enter miles driven: "))
gallons = float(input("Enter gallons of fuel used: "))


# calculations

mpg = miles_driven/gallons


#display results
print("\nProgram Output:")
print(f"{miles_driven:,.2f} miles driven and {gallons:,.2f} gallons used = {mpg:,.2f} mpg") 
