#!/usr/bin/env python3

def get_requirements():
      print("Python Lists")
      print("\nProgram Requirements:\n"
            + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
            + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
            + "3. Create list - using square brackets [list]: my_list = [ 'cherries', 'apples', 'bananas','oranges'].\n"
            + "4. Create a program that mirrors the following IPO (input/process/output) format.\n"
            + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).\n")

def user_input():
      num = 0 

      print("input:")
      num = int(input("Enter number of list elements: "))
      return num

print()

def using_lists(num):

      list = []

      for i in range(num):

            element = input('Please enter list element' + str(i+1)+": ")
            list.append(element)

      print("\nOutput:")
      print("Print my_list:")
      print(list)

      elem = input("\nPlease enter list element: ")
      pos = int(input("Please enter list *index* position(note: must convert to int): "))

      print("\nInsert element into specific position in my_list")
      list.insert(pos,elem)
      print(list)

      print("\nCount number of elements in list: ")
      print(len(list))

      print("\nSort elements in list alphabetically: ")
      list.sort()
      print(list)

      print("\nReverse list: ")
      list.reverse()
      print(list)

      print("\nRemove last list element: ")
      list.pop
      print(list)

      print("\nDelete second element from list by *index* (note: 1=2nd element):")
      del list[1]
      print(list)

      print("\nDelete element from list by *value* (cherries):")
      list.remove('cherries')
      print(list)

      print("\nDelete all elements from list:")
      list.clear()
      print(list)