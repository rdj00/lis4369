#!/usr/bin/env python3

from functions import * 

def main():
      get_requirements()
      list_size = user_input()
      using_lists(list_size)

if __name__ == "__main__":
      main()

