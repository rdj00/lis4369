#!/usr/bin/env python3

from functions import * 

def main():
      get_requirements()
      calculate_calories()

if __name__ == "__main__":
      main() 

