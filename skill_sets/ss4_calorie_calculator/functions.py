#!/usr/bin/env python3

def get_requirements():
      print("\nProgram Requirements:\n"
            + "1. Find calories per grams of fat, carbs, and protein.\n"
            + "2. Calculate percentages\n"
            + "3. Must use float data types.\n"
            + "4. Format, right-align numbers, and round to two decimal places\n")


def calculate_calories():
      print("\nInput:")
      fat = float(input("Enter total fat grams: "))
      carbs = float(input("Enter total carb grams: "))
      protein = float(input("Enter total protein grams: "))

      cal_per_fat = 9 
      cal_per_carb = 4 
      cal_per_protein = 4

      fat = fat * cal_per_fat
      carbs = carbs * cal_per_carb
      protein = protein * cal_per_protein 
      total =  fat + carbs + protein


      fat_percentage = (fat / total) * 100
      carb_percentage = (carbs / total) * 100
      protein_percentage = (protein / total) * 100

      print("\nOutput")
      print("{0:<12} {1:<12} {2:<12}".format("Type", "Calories", "Percentage"))
      print("{0:<12} {1:<12,.2f} {2:>8,.2f} {3}".format("Fat:", fat, fat_percentage, "%"))
      print("{0:<12} {1:<12,.2f} {2:>8,.2f} {3}".format("Carbs:", carbs, carb_percentage, "%"))
      print("{0:<12} {1:<12,.2f} {2:>8,.2f} {3}".format("Protein:", protein, protein_percentage, "%"))


