#!/usr/bin/env python3

# Program requirements
print("Skill Set 1 - Square Feet to Acres")

# Developer: Ryan Johns

# Course: LIS4369

# Semester: Fall 2021 

print("\nProgram Requirements:\n"
      + "1. Research: number of square feet to acre of land\n"
      + "2. Must use float data type for user input and calculation.\n"
      + "3. Format and round conversion to two decimal places\n")

print("\nUser Input:")
square_feet = float(input("Enter square feet: "))


# calculations

acre = square_feet/43560


#display results
print("\nProgram Output:")
print(f"{square_feet:,.2f} square feet = {acre:,.2f} acres") 
