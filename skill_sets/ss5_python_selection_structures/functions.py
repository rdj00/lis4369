#!/usr/bin/env python3

def get_requirements():
      print("\nProgram Requirements:\n"
            + "1. Use Python selection structure.\n"
            + "2. Prompt user for two numbers, and a suitable operator\n"
            + "3. Test for correct numeric operator.\n"
            + "4. Replicate display below.\n")

def test_operation(num1, num2, operator):
      if operator == "+":
            num3 = num1 + num2
            print(num3)
      elif operator == "-":
           num3 = num1 - num2
           print(num3)
      elif operator == "*":
            num3 = num1 * num2
            print(num3)
      elif operator == "**":
            num3 = num1 ** num2
            print(num3)
      elif operator == "/":
            if num2 == 0:
                  print ("Cannot divide by 0")
            else:
                  num3 = num1 / num2
                  print(num3)
      elif operator == "//":
            if num2 == 0:
                  print ("Cannot divide by 0")
            else:
                  num3 = num1 // num2
                  print(num3)
      elif operator == "%":
            if num2 == 0:
                  print ("Cannot divide by 0")
            else:
                  num3 = num1 % num2
                  print(num3)
      else:
            print("Illegal Operator!")






def python_calculator():
      num1 = input("Enter num1: ")
      num2 = input("Enter num2: ")


      print("Suitable Operators: +,-,*,/,// (integer division), % (modulo operator), ** (power)")
      operator = (input("Enter operator:"))

      if num1.isnumeric() == True and num2.isnumeric() == True:
            numone = float (num1)
            numtwo = float (num2)
            test_operation(numone,numtwo,operator)
      
      else:
            print ("Incorrect Operator!")


