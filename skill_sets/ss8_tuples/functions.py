#!/usr/bin/env python3

def get_requirements():
      print("Python Tuples")
      print("\nProgram Requirements:\n"
            + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
            + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
            + "3. Create list - using square brackets [list]: my_list = [ 'cherries', 'apples', 'bananas','oranges'].\n"
            + "4. Create a program that mirrors the following IPO (input/process/output) format.\n"
            + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).\n")

def using_tuples():

      print("\nInput: Hard coded--no user input.")

      tuple1 = ("cherries","apples","bananas","oranges")

      tuple2 = 1,2, "three", "four"

      print("\nOutput:")
      print("Print my_tuple1:")
      print(tuple1)

      print()

      
      print("Print my_tuple2:")
      print(tuple2)


      print()

      fruit1,fruit2,fruit3,fruit4 = tuple1
      print("Print my_tuple1 unpacking:")
      print(fruit1,fruit2,fruit3,fruit4)

      print()
      print("Print third element in my_tuple2:")
      print(tuple2[2])

      print()
      print("Print\"slice\"of my_tuple1(second and third elements):")
      print(tuple1[1:3])

      print()
      print("Reassign my_tuple2 using parentheses.")
      tuple2 = (1,2,3,4)
      print("Print my_tuple2:")
      print(tuple2)

      print()
      print("Reassign my_tuple2 using \"packing\" method(no parentheses).")
      my_tuple2 = 5,6,7,8

      print("Print my_tuple2:")
      print(my_tuple2)

      print()
      print("Print number of elements in my_tuple1:" + str(len(tuple1)))

      print()
      print("Print type of my_tuple1:" + str(type(tuple1)))

      print()
      print("Delete my_tuple1: \nNote: will generate error if trying to print after, as it no longer exists.")
      del tuple1