#!/usr/bin/env python3

from functions import * 

def main():
      get_requirements()
      calculate_volume()


if __name__ == "__main__":
      main()

