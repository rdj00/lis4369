#!/usr/bin/env python3

def get_requirements():
      
      print("\nProgram Requirements:\n"
            + "1.Sets (Python data structure): mutable, heterogenous, unordered sequence of elements, *but* cannot hold duplicate\n"
            + "2. Lists are mutable/changeable--that is, can insert, update, delete.\n"
            + "3. Create list - using square brackets [list]: my_list = [ 'cherries', 'apples', 'bananas','oranges'].\n"
            + "4. Create a program that mirrors the following IPO (input/process/output) format.\n"
            + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).\n")
      print("\nPython Sets")

def using_sets():

      print("Input: Hard coded--no user input.See three examples above.")
      print("***Note***: All three sets below print as \"sets\"(i.e., curly brackets), *regardless* of how they were created!\n")

      my_set = {1,3.14,2.0,'four','Five'}
      print("Print my_set created using curly brackets:")
      print(my_set)

      print("\nPrint type of my_set:")
      print(type(my_set))

      my_set1 = set([1,3.14,2.0,'four','Five'])
      print("\nPrint my_set1 created using set() function with list.")
      print(my_set1)

      print("\nPrint type of my_set1:")
      print(type(my_set1))


      my_set2 = set([1,3.14,2.0,'four','Five'])
      print("\nPrint my_set1 created using set() function with list.")
      print(my_set2)

      print("\nPrint type of my_set2:")
      print(type(my_set2))

      print("\nLength of my_set")
      print(len(my_set))

      print("\nAdd element to set(4) using add() method:")
      my_set.add(4)
      print(my_set)

      print("\nLength of my_set:")
      print(len(my_set))

      print("\nDiscard 'four':")
      my_set.discard('four')
      print(my_set)

      print("\nRemove 'Five':")
      my_set.remove('Five')
      print(my_set)

      print("\nLength of my_set:")
      print(len(my_set))

      print("\nAdd element to set(4) using add() method:")
      my_set.add(4)
      print(my_set)

      print("\nLength of my_set:")
      print(len(my_set))

      print("\nDisplay minimum number:")
      print(min(my_set))

      print("\nDisplay maximum number:")
      print(max(my_set))

      print("\nDisplay sum of numbers:")
      print(sum(my_set))

      print("\nDelete all set elements:")
      my_set.clear()
      print(my_set)

      print("\nLength of my_set:")
      print(len(my_set))
